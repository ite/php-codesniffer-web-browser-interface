<?php
/**
 * Created by Albert van Niekerk
 * User: staalburger
 * Date: 2014/03/18
 * Time: 12:54 PM
 */

	$www_root = $_SERVER['DOCUMENT_ROOT'];
	$handle = opendir($www_root);
	$options_array = array();
	while(false !== ($resource = readdir($handle))) {
		if(is_dir($www_root.DIRECTORY_SEPARATOR.$resource) && $resource != '.' && $resource != '..')
			$options_array[] = "<option value='".$www_root.DIRECTORY_SEPARATOR.$resource."'>{$resource}</option>";
	}
	
	/* TODO: Add validation*/
?>

<html>
<head>
	<link rel='stylesheet' type='text/css' href='assets/bootstrap/css/bootstrap.css'>
	<script type='text/javascript' src='assets/js/jquery.js'></script>
	<script type='text/javascript' src='assets/bootstrap/js/bootstrap.min.js'></script>
</head>
<body>
<nav class="navbar navbar-inverse" role="navigation">
	<div class="container-fluid">
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="step_1">
			<div class="navbar-header">
				<a class="navbar-brand" href="index.php">PHP CodeSniffer</a>
			</div>
			<ul class="nav navbar-nav">
				<li><img src="../web/assets/im/dog.png" alt="codeSniffer" class="img-rounded"></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#">Help</a></li>
				<li><a href="#">Standards</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
<ol class="breadcrumb">
	<li class="active">Select Project & Standard</li>
</ol>
<div class='page-header'>
	<h1>The PHP CodeSniffer browser interface <br><small>making sniffs clean</small></h1>
</div>
<div class='container'>
	<form action='file_to_sniff.php' method='post' enctype='multipart/form-data'>
		<div class="form-group">
			<h3>1. Select the project whose files you want to code sniff </h3>
			<p class='text-muted'><b>Note:</b> The project must contain <i><b>.php</b></i> files</p>
			<select name='project' class="form-control">
				<option value='-1'>- Select a project -</option>
				<?php
				foreach($options_array as $option)
					echo $option;
				?>
			</select>
		</div>
		<div class="form-group">
			<h3>2. Select the standard to be used</h3>
			<select name='standard' class="form-control">
				<option value='0'>Not specified/Generic</option>
				<option value='CodeIgniter'>Code Igniter</option>
				<option value='PEAR'>PEAR</option>
				<option value='PHPCS'>PHPCS</option>
				<option value='PSR1'>PSR1</option>
				<option value='PSR2'>PSR2</option>
				<option value='Zend'>Zend</option>
			</select>
		</div>
		<button type="submit" class="btn btn-success">Next</button>
	</form>
</div>
</body>
</html>
