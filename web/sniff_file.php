<?php
if(isset($_POST['dir']) && isset($_POST['file'])) {
	$file_path = $_POST['dir'];
	$file_name = $_POST['file'];
	
	/* TODO: Add a variable and check to implement the users chosen standard*/

	$sniff_table = decode_feedback($file_path, $file_name);
}
else
{
	$sniff_table = '<h2>Ohw Snap, some stuff just went haywire...<a class="text-danger">Sorry</a></h2>';
}

/**
 * @param $file_path
 * @param $file_name
 *
 * @return string
 */
function decode_feedback($file_path, $file_name) {
	$feedback = '';
	if(strpos($file_path, '.php') !== false)
	{
		// Get the sniff results as XML
		$output = shell_exec("phpcs  -n --standard=CodeIgniter --report=xml {$file_path} ");

		// Decode the XML so that it can be formatted to HTML
		$xml = simplexml_load_string($output);

		// Build the output
		$html = '';
		$html .= "<h4>File: <a class='text-muted'>{$file_name}</a> | Errors: <a class='text-muted'>{$xml->file['errors']}</a></h4>";
		//$html .= "<h4></h4>";

		$html .= "<table class='table table-hover table-striped'>
			<tr>
			<th>Line</th>
			<th>Column</th>
			<th>Type</th>
			<th>Message</th>
			</tr>";

		foreach($xml->file->error as $type => $error_line) {
			$html .= '<tr>';
			$html .= "<td class='text-center'>".$error_line['line'].'</td>';
			$html .= "<td class='text-center'>".$error_line['column'].'</td>';
			$html .= "<td class='text-danger'>".$type.'</td>';
			$html .= "<td>".$error_line.'</td>';
			$html .= '</tr>';
		}

		$html .= '</table>';

		$feedback = $html;
	}
	else
	{
		$feedback = "<h2>The file: {$file_path}, is not a valid php file, please select a valid php file to sniff</h2>";
	}

	return $feedback;
}

?>

<html>
<head>
	<link rel='stylesheet' type='text/css' href='assets/bootstrap/css/bootstrap.css'>
	<script type='text/javascript' src='assets/js/jquery.js'></script>
	<script type='text/javascript' src='assets/bootstrap/js/bootstrap.min.js'></script>
</head>
<body>
<nav class="navbar navbar-inverse" role="navigation">
	<div class="container-fluid">
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="step_1">
			<div class="navbar-header">
				<a class="navbar-brand" href="index.php">PHP CodeSniffer</a>
			</div>
			<ul class="nav navbar-nav">
				<li><img src="../web/assets/im/dog.png" alt="codeSniffer" class="img-rounded"></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#">Help</a></li>
				<li><a href="#">Standards</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
<ol class="breadcrumb">
	<li><a href="index.php">Select Project & Standard</a></li>
	<li><a href="file_to_sniff.php">Select File to sniff</a></li>
	<li class="active">Sniff the file</li>
</ol>
<div class='page-header'>
	<h1>The PHP CodeSniffer browser interface <br><small>making sniffs clean</small></h1>
</div>
<div class='container'>
	<?php echo $sniff_table; ?>
</div>
</body>

