<?php
/**
 * Created by Albert van Niekerk
 * User: staalburger
 * Date: 2014/03/18
 * Time: 12:54 PM
 */

$ignore_list = array('.', '..', '.git', '.idea', '.gitignore', '.htaccess', 'CodeSniffer', 'libraries');

if(isset($_POST['project']) && $_POST['project'] != -1) {
	$dir = $_POST['project'];

	$output = dirToArray($dir, $ignore_list);
	$output = array_remove_empty($output);
	
	/* TODO: Post the standard to sniff by to the sniff_file.php*/
	/* TODO: Add validation*/
}
else
{
	$output = '';
}

function print_data($array_to_print) {
	if(is_array($array_to_print)) {
		foreach ($array_to_print as $key => $sub_array)
		{
			if (is_array($sub_array)) {
				echo "<li><kbd>{$key}</kbd><ul>";
				print_data($sub_array);
			}
			else {
				echo $sub_array;
			}
		}
		echo '</ul></li>';
	}
	else{
		echo '<h4><small class="text-danger">ERROR: </small>No valid Project file selected</h4>';
	}
}

function dirToArray($dir, $ignore) {
	$contents = array();
	# Foreach node in $dir
	foreach (scandir($dir) as $node) {
		if(!in_array(strtolower($node),$ignore)) {
			# Check if it's a node or a folder
			if (is_dir($dir . DIRECTORY_SEPARATOR . $node)) {
				# Add directory recursively, be sure to pass a valid path
				# to the function, not just the folder's name
				$contents[$node] = dirToArray($dir.DIRECTORY_SEPARATOR.$node, $ignore);
			} else if(strpos(strtolower($node), '.php') !== false) {
				# Add node, the keys will be updated automatically
				$contents[] = "<li class='link' id='".$dir.DIRECTORY_SEPARATOR.$node."'>{$node}</li>";
			}
		}
	}
	# done
	return $contents;
}

function array_remove_empty($haystack)
{
	foreach ($haystack as $key => $value) {
		if (is_array($value)) {
			$haystack[$key] = array_remove_empty($haystack[$key]);
		}

		if (empty($haystack[$key])) {
			unset($haystack[$key]);
		}
	}

	return $haystack;
}

?>
<html>
<head>
	<link rel='stylesheet' type='text/css' href='assets/css/basic_styling.css'>
	<link rel='stylesheet' type='text/css' href='assets/bootstrap/css/bootstrap.css'>
	<script type='text/javascript' src='assets/js/jquery.js'></script>
	<script type='text/javascript' src='assets/js/file_structure.js'></script>
	<script type='text/javascript' src='assets/bootstrap/js/bootstrap.min.js'></script>
</head>
<body>
<nav class="navbar navbar-inverse" role="navigation">
	<div class="container-fluid">
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="step_1">
			<div class="navbar-header">
				<a class="navbar-brand" href="index.php">PHP CodeSniffer</a>
			</div>
			<ul class="nav navbar-nav">
				<li><img src="../web/assets/im/dog.png" alt="codeSniffer" class="img-rounded"></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#">Help</a></li>
				<li><a href="#">Standards</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
<ol class="breadcrumb">
	<li><a href="index.php">Select Project & Standard</a></li>
	<li class="active">Select File to sniff</li>
</ol>
<div class='page-header'>
	<h1>The PHP CodeSniffer browser interface <br><small>making sniffs clean</small></h1>
</div>
<div class='container'>
	<div id="listContainer">
		<ul class='sniff_links' id="expList">
			<?php print_data($output); ?>
		</ul>
	</div>
</div>
<form id='selected_file' action='sniff_file.php' method='post' enctype='multipart/form-data'>
	<div class="form-group">
		<input type='hidden' name='dir' id='dir' value="">
		<input type='hidden' name='file' id='file'>
	</div>
</form>
</body>