$(function(){
	//IMPORTANT INFO | Source: http://jasalguero.com/ledld/development/web/expandable-list/
	function prepareList() {
		$('#expList').find('li:has(ul)')
			.click( function(event) {
				if (this == event.target) {
					$(this).toggleClass('expanded');
					$(this).children('ul').toggle('medium');
				}
				return false;
			})
			.addClass('collapsed')
			.children('ul').hide();
	};

	function send_link_to_sniff() {
		$('ul.sniff_links li.link').click(function(e)
		{
			var file_dir = $(this).attr('id');
			var file_name = $(this).html();

			$('#dir').val(file_dir);
			$('#file').val(file_name);

			$('#selected_file').submit();
		});
	}

	$(document).ready( function() {
		prepareList();
		send_link_to_sniff();
	});
})