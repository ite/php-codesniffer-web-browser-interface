php-codeSniffer-browser-interface
A tool that allows you to sniff your php project files from within your browser.

Note:
 - The code still needs validation.
 - Currently the codeIgniter standard is the default and only option to sniff code by.
	 Next release will have option to use other specified standards.